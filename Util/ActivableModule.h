/*
 * ActivableModule.h
 *
 *  Created on: 30 avr. 2021
 *      Author: nicolas-demailly
 */

#ifndef UTIL_ACTIVABLEMODULE_H_
#define UTIL_ACTIVABLEMODULE_H_

#include "../Module/Key.h";
#include "../Util/MidiModule.h";
#include "../Util/Selectable.h";

namespace util {

	class ActivableModule : public MidiModule, public util::Selectable {
		private:
			Key* key;
			bool active = true;
			int save = -1;

		public :
			ActivableModule(Key* activeKey, Key* selectKey) : Selectable(selectKey) {
				this->key = activeKey;
			}

			inline void enable() {
				this->active = true;
				MidiModule::setMidiValue(this->save);
				this->key->getLed()->setColor(Color::ACTIVE);
			}

			inline void disable() {
				this->active = false;
				this->save = MidiModule::getMidiValue();
				this->key->getLed()->setColor(Color::OFF);
			}

			inline void toogle() {
				if (this->active) {
					this->disable();
				} else {
					this->enable();
				}
			}

	};

}

#endif /* UTIL_ACTIVABLEMODULE_H_ */

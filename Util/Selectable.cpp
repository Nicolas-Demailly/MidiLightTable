/*
 * SelectableModule.cpp
 *
 *  Created on: 30 avr. 2021
 *      Author: nicolas-demailly
 */

#include "Selectable.h"
#include "../Asset/Screen.h"

namespace util {
	static std::list<Selectable*> list, selected;
	static bool multi = false;

	Selectable::Selectable(SelectableType type, Key* key) {
		this->type = type;
		this->key = key;
		list.push_back(this);
		if (!(key == nullptr)) {
			key->setEvent([](Key* key, KeyEvent event, void* something) {

				if (event.getType() == KeyEvent::CONTINUE && event.since() > 500) {
					multi = true;
				}
				if (!multi) unselectAll();
				if ((selected.size() > 1 && multi) || !multi) {
					selected.push_back(static_cast<Selectable*>(something));
				}

			}, this);
		}
	}

	void Selectable::select() {
		selected.push_back(this);
		this->key->getLed()->setColor(Color::SELECTED);
		asset::screen::update();
	}

	void Selectable::unselect() {
		selected.remove(this);
		this->key->getLed()->setColor(Color::NORMAL);
		asset::screen::update();
		/**
		 * Si ça marche pas :
		 *
		 * vector.erase(std::remove_if(vector.begin(), vector.end(), [&](auto & item) { return item == "toto"; }, vector.end());
		 *
		 */ //TOTEST
	}

	std::list<Selectable*> getSelected() {
		return selected;
	}

	void selectAll() {
		for (Selectable* &a : list) a->select();
	}

	void unselectAll() {
		for (Selectable* &a : selected) a->unselect();
	}

	static void SelectableKey::interact(Key* key, KeyEvent event, void* something) {
		if (event.getType() == KeyEvent::CONTINUE) {
			if (event.since() > 1000) {
				key->getLed()->setColor(Color::SOON_SELECTED_MULTI);
			} else if (event.since() > 500) {
				key->getLed()->setColor(Color::SOON_SELECTED_SINGLE);
			}
		}
		if (event.getType() == KeyEvent::RELEASE) {
			if (event.since() > 1000) {
				multi = true;
			}
			if (event.since() > 500) {
				if (!multi) unselectAll();
				if ((selected.size() > 1 && multi) || !multi) {
					selected.push_back(static_cast<Selectable*>(something));
				}
			}
		}
		SelectableKey* a = static_cast<SelectableKey*>(something);
		a->event(key, event, a->something);
	}

}

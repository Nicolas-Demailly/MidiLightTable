/*
 * MidiModule.cpp
 *
 *  Created on: 30 avr. 2021
 *      Author: nicolas-demailly
 */

#include "MidiModule.h"

namespace util {

	static std::vector<MidiModule*> modules;

	std::vector<MidiModule*> MidiModule::getModules() {
		return modules;
	}

	MidiModule::MidiModule(int midiAddress) {
		this->midiAddress = midiAddress;
		modules.push_back(this);
	}

	void MidiModule::midiKey(Key* key, KeyEvent event, void *something) {
		MidiModule module = static_cast<MidiModule>(something);
		module.setMidiValue((event.getType() == KeyEvent::RELEASE) ? 0 : 127);
	}
}

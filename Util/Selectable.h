/*
 * SelectableModule.h
 *
 *  Created on: 30 avr. 2021
 *      Author: nicolas-demailly
 */

#ifndef UTIL_SELECTABLE_H_
#define UTIL_SELECTABLE_H_

#include "../Module/Key.h"
#include <list>
#include <map>

namespace util {

	enum class SelectableType {
		SLIDER, NOT_TOGGLEABLE
	};

	class Selectable {
		private:
			SelectableType type;
			Key* key;

		public:

			Selectable(util::SelectableType type, Key* key);
			Selectable(Key* key) : Selectable(util::SelectableType::NOT_TOGGLEABLE, key) {
			}
			Selectable() : Selectable(nullptr) {
			}

			virtual ~Selectable() {

			}

			static void work(Key* key, KeyEvent event);

			inline bool is(SelectableType type) {
				return this->type == type;
			}

			void select();
			void unselect();
	};

	class SelectableKey : public Selectable, public Key {
		static void interact(Key* key, KeyEvent event, void* something);

		void (*event)(Key*, KeyEvent, void*);
		void *something;

		SelectableKey(Address address, Led* led, void (*event)(Key*, KeyEvent, void*), void *something) : Key(address, led, interact, this) {
			this->event = event;
			this->something = something;
		}

		SelectableKey(Address address, Led* led, void (*event)(Key*, KeyEvent, void*)) : SelectableKey(address, led, event, nullptr) {

		}

		SelectableKey(Address address, Led* led) : SelectableKey(address, led, nullptr) {

		}

		inline void setEvent(void (*event)(Key*, KeyEvent, void*), void* something) {
			this->something = something;
			this->event = event;
		}

		virtual ~SelectableKey() {

		}
	};

	void selectAll();
	void unselectAll();
	std::list<Selectable*> getSelected();
}

#endif /* UTIL_SELECTABLE_H_ */

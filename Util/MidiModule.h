/*
 * MidiModule.h
 *
 *  Created on: 30 avr. 2021
 *      Author: nicolas-demailly
 */

#ifndef UTIL_MIDIMODULE_H_
#define UTIL_MIDIMODULE_H_

#include "../Module/Key.h"

namespace util {

	class MidiModule {
		private:
			int midiAddress, midiValue = 0;
			bool changed = false;

		public:
			MidiModule(int midiAddress);

			MidiModule() : MidiModule(-1) {

			}

			inline void setMidiAddress(int midiAddress) {
				this->midiAddress = midiAddress;
			}

			inline int getMidiAddress() {
				return this->midiAddress;
			}

			inline void setMidiValue(int value) {
				this->changed = true;
				this->midiValue = value;
			}

			inline int readMidiValue() { // Use only in main loop
				this->changed = false;
				return this->getMidiValue();
			}

			inline int getMidiValue() {
				return this->midiValue;
			}

			//static void setMidiValue(int address, int value);
			static void midiKey(Key* key, KeyEvent event, void* something);
			static std::vector<MidiModule*> getModules();
	};


	class MidiKey: public Key, public MidiModule {
		public:
			MidiKey(Address address, Led* led, void (*event)(Key*, KeyEvent, void*), void *something) : Key(address, led, event, something), MidiModule() {

			}
	};


}

#endif /* UTIL_MIDIMODULE_H_ */

#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2021-05-03 22:47:43

#include "Arduino.h"
#include "Arduino.h"
#include <EEPROM.h>
#include <Adafruit_NeoPixel.h>
#include "Module/Module.h"
#include "Module/Potentiometer.h"
#include "Module/Key.h"
#include "Module/Led.h"
#include "Module/NumericEncoder.h"
#include "Asset/Memory.h"
#include "Util/MidiModule.h"

void resetMux() ;
void setup() ;
void loop() ;

#include "MidiLightTable.ino"


#endif

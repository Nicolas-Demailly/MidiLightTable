/*
 * Numpas.cpp
 *
 *  Created on: 23 avr. 2021
 *      Author: nicolas-demailly
 */

#include "Memory.h"

#include "../Module/Module.h"
#include "../Module/Led.h"
#include "Numpad.h"

namespace asset::memory { //TODO
	Key* key = nullptr;

	void setupLedOf(Key* key) {
		key->getLed()->setColor((getMemory(key) == -1) ? Color::EXISTS : Color::NOT_EXISTS);
	}

	void interact(Key* key, KeyEvent event, void* useless) {
		int time = 1500;
		if (event.getType() == KeyEvent::CONTINUE && event.since() > time) {
			key->getLed()->setColor(Color::CURRENT_CHANGE);
			asset::memory::key = key;
			asset::numpad::requestNumber(15, setMemory);
		} else if (event.getType() == KeyEvent::RELEASE && event.since() < time) {
			//readMemory(getMemory(key));
		}
	}

	void setMemory(int memory) {
		//TODO
	}

	static std::vector<MemoryModule*> memoryModules;

	std::vector<MemoryModule*> getMemoryModules() {
		return memoryModules;
	}

	MemoryModule::MemoryModule() {
		asset::memory::memoryModules.push_back(this);
	}

	Led leds[] = { // Led keys difinitions
		Led(0),		// LOAD
		Led(1),		// SAVE (state)
		Led(2),		// CLEAR
		Led(3),		// CLONE
		Led(4),		// ROLLBACK
		Led(5),		// ???

		Led(6),		// Speed memory 1
		Led(7),		// Speed memory 2
		Led(8),		// Speed memory 3
		Led(9),		// Speed memory 4
		Led(10),	// Speed memory 5
		Led(11),	// Speed memory 6
		Led(10),	// Speed memory 7
		Led(11),	// Speed memory 8
	};

	Key keys[] = { // Keys definitions

		Key(Address(26, 0), &leds[0], ([](Key* key, KeyEvent event, void* useless) {		// LOAD

		})),
		Key(Address(26, 1), &leds[1], ([](Key* key, KeyEvent event, void* useless) {		// SAVE (state)

		})),
		Key(Address(26, 0), &leds[0], ([](Key* key, KeyEvent event, void* useless) {		// CLEAR

		})),
		Key(Address(26, 1), &leds[1], ([](Key* key, KeyEvent event, void* useless) {		// CLONE

		})),
		Key(Address(26, 0), &leds[0], ([](Key* key, KeyEvent event, void* useless) {		// ROLLBACK

		})),
		Key(Address(26, 1), &leds[1], ([](Key* key, KeyEvent event, void* useless) {		// ??

		})),
		Key(Address(26, 4), &leds[4], interact),						// Speed memory 1
		Key(Address(26, 5), &leds[5], interact),						// Speed memory 2
		Key(Address(26, 6), &leds[6], interact),						// Speed memory 3
		Key(Address(26, 7), &leds[7], interact),						// Speed memory 4
		Key(Address(26, 8), &leds[8], interact),						// Speed memory 5
		Key(Address(26, 9), &leds[9], interact),						// Speed memory 6
		Key(Address(26, 10), &leds[10], interact),						// Speed memory 7
		Key(Address(26, 11), &leds[11], interact)						// Speed memory 8

	};

}

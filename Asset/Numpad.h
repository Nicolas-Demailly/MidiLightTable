/*
 * Numpas.h
 *
 *  Created on: 23 avr. 2021
 *      Author: nicolas-demailly
 */

#ifndef ASSET_NUMPAD_H_
#define ASSET_NUMPAD_H_

#include "../Module/Key.h"

namespace asset { namespace numpad {

	void confirmRequest();
	void cancelRequest();
	void requestNumber(int max, void (*event)(int));
	void numericKey(Key* key, KeyEvent event);
	void updateScreen();

}}

#endif /* ASSET_NUMPAD_H_ */

/*
 * Memory.h
 *
 *  Created on: 23 avr. 2021
 *      Author: nicolas-demailly
 */

#ifndef ASSET_MEMORY_H_
#define ASSET_MEMORY_H_

#include "../Module/Key.h"
#include "Numpad.h"

namespace asset { namespace memory {
	static const int MEMORY_SIZE = 64;
	static int memory = 1;
	static bool values[MEMORY_SIZE * 8];

	int getMemory(Key* key);
	void setMemory(int memory);
	void setupMemory(int memory);
	void changeMemory(int memory);
	int readMemory(int from, int to);
	void updateMemory(int from, int to, int value);
	void setupLedOf(Key* key);
	void interact(Key* key, KeyEvent event);
	std::vector<MemoryModule*> getMemoryModules();

}}

#endif /* ASSET_NUMPAD_H_ */

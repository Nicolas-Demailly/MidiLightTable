/*
 * Slider.cpp
 *
 *  Created on: 30 avr. 2021
 *      Author: nicolas-demailly
 */

#include "Slider.h"


namespace asset::slider {
	static std::vector<Slider*> sliders;

	Slider::Slider(module::DynamicPotentiometer* potentiometer, util::MidiKey* selectKey, util::MidiKey* activeKey, util::MidiKey* playKey, util::MidiKey* pauseKey) :
			util::ActivableModule(activeKey, selectKey) {
		this->potentiometer = potentiometer;
		this->playKey = playKey;
		this->pauseKey = pauseKey;
		this->channel = CrossoverChannel::NONE;
		this->state = SliderState::OFF;
		playKey->setEvent(midiKey, playKey);
		pauseKey->setEvent([](Key* key, KeyEvent event, void* something) {
			static_cast<Slider*>(something)->pause(event);
		}, this);
	}

	void Slider::saveMemory() {

	}

	void Slider::updateMemory() {

	}

	std::vector<Slider*> getSliders() {
		return sliders;
	}
}

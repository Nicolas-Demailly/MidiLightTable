/*
 * Slider.h
 *
 *  Created on: 30 avr. 2021
 *      Author: nicolas-demailly
 */

#ifndef ASSET_SLIDER_H_
#define ASSET_SLIDER_H_

#include "../Util/Selectable.h"
#include "../Util/ActivableModule.h"
#include "../Util/MidiModule.h"
#include "../Module/Module.h"
#include "../Module/Potentiometer.h"

#include <vector>

namespace asset::slider {

	enum class CrossoverChannel {
		NONE, A, B
	};

	enum class SliderState {
		OFF = 0, PLAY = 1, PAUSED = 2
	};

	class Slider : public module::MemoryModule, public util::ActivableModule {
		private:
			module::DynamicPotentiometer* potentiometer;
			util::MidiKey *playKey, *pauseKey;
			CrossoverChannel channel;
			SliderState state;

		public:
			Slider(module::DynamicPotentiometer* potentiometer, util::MidiKey* select, util::MidiKey* play, util::MidiKey* pause, util::MidiKey* active);

			virtual ~Slider() {

			}

			inline void setCrossoverChannel(CrossoverChannel channel) {
				this->channel = channel;
			}

			void saveMemory();
			void updateMemory();

			inline void play(KeyEvent event) {
				this->playKey->callEvent(event, this->playKey);
			}

			inline void pause(KeyEvent event) {
				switch (this->state) {
					case SliderState::OFF:
						return;
					case SliderState::PLAY:
						this->pauseKey->setMidiAddress(0);
						return;
					case SliderState::PAUSED:
						this->pauseKey->setMidiAddress(1);
						return;
				}
				util::MidiModule::midiKey(this->pauseKey, event, this->pauseKey);
			}

			inline int getMidiAddress() {
				return util::MidiModule::getMidiAddress();
			}

			inline void setMidiAddress(int midiAddress) {
				util::MidiModule::setMidiAddress(midiAddress);
			}

	};

	std::vector<Slider*> getSliders();
}

#endif /* ASSET_SLIDER_H_ */

/*
 * Screen.h
 *
 *  Created on: 23 avr. 2021
 *      Author: nicolas-demailly
 */

#ifndef ASSET_SCREEN_H_
#define ASSET_SCREEN_H_

namespace asset { namespace screen {

	void clear();
	void showNumber(int number);
	inline void update() { // TODO

	}

}}

#endif /* ASSET_SCREEN_H_ */

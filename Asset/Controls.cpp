/*
 * Controls.cpp
 *
 *  Created on: 30 avr. 2021
 *      Author: nicolas-demailly
 */

#include "Controls.h"

#include "../Util/Selectable.h"
#include "../Util/ActivableModule.h"
#include "Slider.h"

namespace asset {

	namespace controls {

		Led leds[] = {
			Led(0),
			Led(0),
			Led(0),
			Led(0),
			Led(0),
			Led(0)
		};

		Key keys[] = { // TODO Address

			Key(Address(0, 0), &leds[0], ([](Key* key, KeyEvent event, void* useless) {		// Play
				for (util::Selectable* &a : util::getSelected()) {
					if (a->is(util::SelectableType::SLIDER)) {
						static_cast<asset::slider::Slider*>(a)->play(event);
					}
				}
			})),
			Key(Address(0, 0), &leds[0], ([](Key* key, KeyEvent event, void* useless) {		// Pause
				for (util::Selectable* &a : util::getSelected()) {
					if (a->is(util::SelectableType::SLIDER)) {
						static_cast<asset::slider::Slider*>(a)->pause(event);
					}
				}
			})),
			Key(Address(0, 0), &leds[0], ([](Key* key, KeyEvent event, void* useless) {		// Next
				for (util::Selectable* &a : util::getSelected()) {
					if (a->is(util::SelectableType::SLIDER)) {
						asset::slider::Slider* b = static_cast<asset::slider::Slider*>(a);
						int add = b->getMidiAddress() + 1;
						b->setMidiAddress(add);
						util::MidiModule::midiKey(key, event, b);
					}
				}
			})),
			Key(Address(0, 0), &leds[0], ([](Key* key, KeyEvent event, void* useless) {		// Previous
				for (util::Selectable* &a : util::getSelected()) {
					if (a->is(util::SelectableType::SLIDER)) {
						asset::slider::Slider* b = static_cast<asset::slider::Slider*>(a);
						int add = b->getMidiAddress() + 1;
						b->setMidiAddress(add);
						util::MidiModule::midiKey(key, event, b);
					}
				}
			})),
			Key(Address(0, 0), &leds[0], ([](Key* key, KeyEvent event, void* useless) {		// Select all + Enable
				if (event.getType() == KeyEvent::CONTINUE && event.since() > 500) {
					util::selectAll();
				} else {
					for (util::Selectable* &a : util::getSelected()) {
						if (!a->is(util::SelectableType::NOT_TOGGLEABLE)) {
							static_cast<util::ActivableModule*>(a)->enable();
						}
					}
				}
			})),
			Key(Address(0, 0), &leds[0], ([](Key* key, KeyEvent event, void* useless) {		// Unselect all + Disable
				if (event.getType() == KeyEvent::CONTINUE && event.since() > 500) {
					util::unselectAll();
				} else {
					for (util::Selectable* &a : util::getSelected()) {
						if (!a->is(util::SelectableType::NOT_TOGGLEABLE)) {
							static_cast<util::ActivableModule*>(a)->disable();
						}
					}
				}
			})),
			Key(Address(0, 0), &leds[0], ([](Key* key, KeyEvent event, void* useless) {		// Reset crossfader (selected)
				for (util::Selectable* &a : util::getSelected()) {
					if (a->is(util::SelectableType::SLIDER)) {
						static_cast<asset::slider::Slider*>(a)->setCrossoverChannel(asset::slider::CrossoverChannel::NONE);
					}
				}
			})),
			Key(Address(0, 0), &leds[0], ([](Key* key, KeyEvent event, void* useless) {		// Reset all crossfader
				for (asset::slider::Slider* &a : asset::slider::getSliders()) {
					a->setCrossoverChannel(asset::slider::CrossoverChannel::NONE);
				}
			})),
			Key(Address(0, 0), &leds[0], ([](Key* key, KeyEvent event, void* useless) {		// Set cross A
				for (util::Selectable* &a : util::getSelected()) {
					if (a->is(util::SelectableType::SLIDER)) {
						static_cast<asset::slider::Slider*>(a)->setCrossoverChannel(asset::slider::CrossoverChannel::A);
					}
				}
			})),
			Key(Address(0, 0), &leds[0], ([](Key* key, KeyEvent event, void* useless) {		// Set cross B
				for (util::Selectable* &a : util::getSelected()) {
					if (a->is(util::SelectableType::SLIDER)) {
						static_cast<asset::slider::Slider*>(a)->setCrossoverChannel(asset::slider::CrossoverChannel::B);
					}
				}
			})),


		};


	}

}


/*
 * Numpas.cpp
 *
 *  Created on: 23 avr. 2021
 *      Author: nicolas-demailly
 */

#include "Numpad.h"

#include "Arduino.h"

#include "Screen.h"
#include "../Module/Key.h"
#include "../Module/Led.h"
#include "../Module/NumericEncoder.h"

namespace asset { namespace numpad {
	int number = 0, max = -1;
	void (*event)(int) = nullptr;

	void confirmRequest() {
		if (event) {
			event(number);
			event = nullptr;
			screen::clear();
		}
		number = 0;
	}

	void cancelRequest() {
		number = -1;
		confirmRequest();
	}

	void requestNumber(int max, void (*event)(int)) {
		cancelRequest();
		numpad::max = max;
		numpad::event = event;
		screen::clear();
	}

	void checkValue() {
		if (number > max) number = max;
	}

	void updateScreen() {
		if (numpad::event && number > 0) screen::showNumber(number);
	}

	void numericKey(Key* key, KeyEvent event, void* n) {
		int power = 0;
		while (number > pow(10, power)) power++;
		number += pow(10, power) * *static_cast<int*>(n);
		checkValue();
		updateScreen();
	}


	Led leds[] = { // Led keys difinitions
		Led(0),		// ENTER
		Led(1),		// CANCEL
		Led(2),		// 0
		Led(3),		// 1
		Led(4),		// 2
		Led(5),		// 3
		Led(6),		// 4
		Led(7),		// 5
		Led(8),		// 6
		Led(9),		// 7
		Led(10),	// 8
		Led(11),	// 9
	};

	int numbers[] = {0,1,2,3,4,5,6,7,8,9};

	Key keys[] = { // Keys definitions

		Key(Address(26, 0), &leds[0], ([](Key* key, KeyEvent event, void* useless) {		// ENTER
			confirmRequest();
		})),
		Key(Address(26, 1), &leds[1], ([](Key* key, KeyEvent event, void* useless) {		// CANCEL
			cancelRequest();
		})),
		Key(Address(26, 2), &leds[2], numericKey, &numbers[0]),						// 0
		Key(Address(26, 3), &leds[3], numericKey, &numbers[1]),						// 1
		Key(Address(26, 4), &leds[4], numericKey, &numbers[2]),						// 2
		Key(Address(26, 5), &leds[5], numericKey, &numbers[3]),						// 3
		Key(Address(26, 6), &leds[6], numericKey, &numbers[4]),						// 4
		Key(Address(26, 7), &leds[7], numericKey, &numbers[5]),						// 5
		Key(Address(26, 8), &leds[8], numericKey, &numbers[6]),						// 6
		Key(Address(26, 9), &leds[9], numericKey, &numbers[7]),						// 7
		Key(Address(26, 10), &leds[10], numericKey, &numbers[8]),
		Key(Address(26, 11), &leds[11], numericKey, &numbers[9]) // REST 4 (12,13,14,15)

	};

	/*NumericEncoder encoder = NumericEncoder([](NumericEncoder* encoder, int value) {
		number += value;
		if (number < 1) number = 1;
		checkValue();
		updateScreen();
	});*/

}}

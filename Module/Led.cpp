/*
 * Led.cpp
 *
 *  Created on: 21 avr. 2021
 *      Author: nicolas-demailly
 */

#include <iostream>
#include "Led.h"

namespace module {

	Color // R G B Strobe (ms 50/50)

	Color::OFF = Color(0,0,0,0),
	Color::RED = Color(255,0,0,0),
	Color::GREEN = Color(0,255,0,0),
	Color::BLUE = Color(0,0,255,0),
	Color::CURRENT_CHANGE = Color(0,0,255,100),
	Color::EXISTS = Color(0,0,255,0),
	Color::NOT_EXISTS = Color(0,0,255,0),
	Color::SELECTED = Color(0,255,255,0),
	Color::NORMAL = Color(0,0,255,0),
	Color::ACTIVE = Color(255,255,255,0);

}

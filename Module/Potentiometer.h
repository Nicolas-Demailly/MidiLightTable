/*
 * Slider.h
 *
 *  Created on: 21 avr. 2021
 *      Author: nicolas-demailly
 */

#ifndef MODULE_POTENTIOMETER_H_
#define MODULE_POTENTIOMETER_H_

#include "Module.h"
#include "../Util/MidiModule.h"

using namespace module;

namespace module {

	class Potentiometer: public AddressableModule, util::MidiModule {

		public:
			Potentiometer(Address address) : AddressableModule(address) {

			}

			void work();

	};

	class DynamicPotentiometer: public Potentiometer, public MemoryModule {

		private:
			Address motor;

		public:
			DynamicPotentiometer(Address module, Address motor) : Potentiometer(module) {
				this->motor = motor;
			}

			void work();

		void saveMemory();
		void updateMemory();
	};

}

#endif /* MODULE_POTENTIOMETER_H_ */

/*
 * NumericEncoder.h
 *
 *  Created on: 21 avr. 2021
 *      Author: nicolas-demailly
 */

#ifndef MODULE_NUMERICENCODER_H_
#define MODULE_NUMERICENCODER_H_

#include "Module.h"

using namespace module;

namespace module {

	class NumericEncoder: public Module { // TODO
		private :
			Address a1, a2;
			int v1 = -1, v2 = -1, last = -1;
			void (*event)(NumericEncoder*, int);

		public:
			NumericEncoder(Address a1, Address a2, void (*event)(NumericEncoder*, int)) : Module() {
				this->a1 = a1;
				this->a2 = a2;
				this->event = event;
			}

			void work();

	};

	/*class MidiNumericEncoder: public NumericEncoder, MidiModule { // TODO
		public:
			MidiNumericEncoder(Address a1, Address a2) : NumericEncoder(a1, a2, here) {

			}



			void work();

	};*/

}

#endif /* MODULE_NUMERICENCODER_H_ */

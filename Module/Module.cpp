#include "Module.h"

namespace module {

	static std::vector<Module*> modules;

	std::vector<Module*> getModules() {
		return modules;
	}

	void addModule(Module* module) {
		modules.push_back(module);
	}

	Module::Module() {
		addModule(this);
	}

}

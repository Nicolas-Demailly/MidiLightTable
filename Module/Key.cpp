#include "Key.h"

namespace module {
	int

	KeyEvent::PRESS = 0,
	KeyEvent::CONTINUE = 1,
	KeyEvent::RELEASE = 2;

	void Key::work() {
		int value = readDigitalValue();
		if (value == 0) {
			time++;
			if (time == 0) {
				this->callEvent(KeyEvent(KeyEvent::PRESS, 0));
			} else {
				this->callEvent(KeyEvent(KeyEvent::CONTINUE, this->time));
			}
		} else if (time >= 0) {
			this->callEvent(KeyEvent(KeyEvent::RELEASE, this->time));
			this->time = -1;
		}
	}


}

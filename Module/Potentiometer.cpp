#include "Potentiometer.h"

namespace module {

	void Potentiometer::work() {
		this->readAnalogValue();
	}

	void DynamicPotentiometer::work() {
		Potentiometer::work();
	}

	void DynamicPotentiometer::updateMemory() {

	}

	void DynamicPotentiometer::saveMemory() {

	}
}

/*
 * Led.h
 *
 *  Created on: 21 avr. 2021
 *      Author: nicolas-demailly
 */

#ifndef LED_H_
#define LED_H_

#include "Module.h"

using namespace module;

namespace module {

	class Color {
		private:
			int r, g, b, s;

		public:
			Color(int r, int g, int b, int s) {
				this->r = r;
				this->g = g;
				this->b = b;
				this->s = s;
			}

			inline bool operator==(const Color other) {
				return this->r == other.r
						&& this->g == other.g
						&& this->b == other.b
						&& this->s == other.s;
			}

			inline int getRed() {
				return this->r;
			}

			inline int getGreen() {
				return this->g;
			}

			inline int getBlue() {
				return this->b;
			}

			inline int getStrobe() {
				return this->s;
			}

		static Color OFF, RED, GREEN, BLUE, CURRENT_CHANGE, EXISTS, NOT_EXISTS, SELECTED, NORMAL, ACTIVE, SOON_SELECTED_MULTI, SOON_SELECTED_SINGLE;
	};

	class Led : public module::Module {
		private:
			int address;
			double brightness = 1;
			bool strobe = true;
			long time = 0;
			Color color = Color::OFF, last = Color::OFF;

		public:
			Led() : Module(false) { // @suppress("Class members should be properly initialized")

			}

			Led(int address) : Module() {
				this->address = address;
			}

			inline int getAddress() {
				return this->address;
			}

			inline double getBrightness() {
				return this->brightness;
			}

			inline void setBrightness(double brightness) {
				this->brightness = brightness;
			}

			inline void setColor(Color color) {
				this->color = color;
			}

			inline void turnOff() {
				this->color = Color::OFF;
			}

			inline Color getColor() {
				return this->color;
			}

			void work();
	};

}

#endif /* LED_H_ */

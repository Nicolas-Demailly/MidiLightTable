/*
 * Key.h
 *
 *  Created on: 21 avr. 2021
 *      Author: nicolas-demailly
 */

#ifndef MODULE_KEY_H_
#define MODULE_KEY_H_

#include "Module.h"
#include "Led.h"

using namespace module;

namespace module {

	class KeyEvent {
		private:
			int t, s; // 0 = Press; 1 = Continue...; 2 = Release

		public:
			static int PRESS, CONTINUE, RELEASE;

			KeyEvent(int type, int since) {
				this->t = type;
				this->s = since;
			}

			inline int getType() {
				return this->t;
			}

			inline int since() {
				return this->s;
			}
	};

	class Key: public AddressableModule {
		private:
			Led* led;
			void (*event)(Key*, KeyEvent, void*);
			void *something;
			long time = -1;

		public:
			Key(Address address, Led* led, void (*event)(Key*, KeyEvent, void*), void *something) : AddressableModule(address) {
				this->led = led;
				this->event = event;
				this->something = something;
			}

			Key(Address address, Led* led, void (*event)(Key*, KeyEvent, void*)) : Key(address, led, event, nullptr) {

			}

			Key(Address address, Led* led) : Key(address, led, nullptr) {

			}

			inline void setEvent(void (*event)(Key*, KeyEvent, void*), void* something) {
				this->something = something;
				this->event = event;
			}

			inline void callEvent(KeyEvent event, void* something) {
				this->event(this, event, something);
			}

			inline void callEvent(KeyEvent event) {
				this->event(this, event, this->something);
			}

			inline Led* getLed() {
				return this->led;
			}

			void work();
	};

}

#endif /* MODULE_KEY_H_ */

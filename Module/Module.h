/*
 * Module.h
 *
 *  Created on: 21 avr. 2021
 *      Author: nicolas-demailly
 */

#ifndef MODULE_H_
#define MODULE_H_

#include <vector>

namespace module {

	class Module {

		public:
			Module();
			Module(bool) {
				// Ignore
			}
			virtual ~Module() {}
			virtual void work() = 0;

	};

	std::vector<Module*> getModules();
	void addModule(Module* module);

	class Address {
		private:
			int port, address;

		public:
			inline Address() : Address(0,0) {

			}

			inline Address(int port, int address) {
				this->port = port;
				this->address = address;
			}

			inline int getPort() {
				return this->port;
			}

			inline int getAddress() {
				return this->address;
			}
	};

	class AddressableModule : public Module {

		private:
			Address address;
			int value = 0;

		public:
			AddressableModule(Address address) : Module() {
				this->address = address;
			}

			inline Address getAddress() {
				return this->address;
			}

			inline int getValue() {
				return this->value;
			}

			inline void setValue(int value) {
				this->value = value;
			}

			int readAnalogValue();
			int readDigitalValue();
			virtual void work() = 0;
	};

	class MemoryModule {
		public:
			MemoryModule();

			virtual ~MemoryModule() {

			}

			virtual void saveMemory();
			virtual void updateMemory();
	};

}

#endif /* MODULE_H_ */

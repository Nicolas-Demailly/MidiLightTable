#include "Arduino.h"
#include <EEPROM.h>

#include <Adafruit_NeoPixel.h>

#include "Module/Module.h"
#include "Module/Potentiometer.h"
#include "Module/Key.h"
#include "Module/Led.h"
#include "Module/NumericEncoder.h"
#include "Asset/Memory.h"
#include "Util/MidiModule.h"

int mux[5], muxAddress;
double globalBrightness = 1;

void resetMux() {
	for (int i = 0; i < (*(&mux + 1) - mux); i++) mux[i] = 0;
	muxAddress = 0;
}

//Potentiometer p1 = Potentiometer(Address(A0, 0));
//Potentiometer p2 = Potentiometer(Address(A0, 1));

NumericEncoder encoder = NumericEncoder(Address(30, 0), Address(30, 1), ([](NumericEncoder* enco, int v) {		// CANCEL
	Serial.println(v);
}));

/*Led led = Led(0);
Led led1 = Led(1);

Key key = Key(Address(30, 1), &led, ([](Key* key, KeyEvent event) {		// ENTER
	if (event.getType() == 0) {
		key->getLed()->setColor(Color(255,0,0,0));
	} else 	if (event.getType() == 2) {
		key->getLed()->turnOff();
	}
}));*/

Adafruit_NeoPixel leds = Adafruit_NeoPixel(2, 20, NEO_RGB + NEO_KHZ800);

void setup() {
	Serial.begin(115200);

	// External power input check
	pinMode(12, INPUT); // 5V from external power input
	pinMode(13, OUTPUT); // Relay
	delay(100);
	while (!(digitalRead(12) == HIGH)) {
		// WAIT !!
		delay(100);
	}
	pinMode(13, HIGH);
	delay(500);
	// --

	for (int i = 0; i < 4; i++) pinMode(22 + i, OUTPUT);
	resetMux();
	leds.begin();
	leds.setBrightness(100);
	asset::memory::changeMemory(1);
}

void loop() {
	// Mux & modules work functions
	while (mux[4] != 1) {
		for (module::Module* &module : module::getModules()) module->work();
		// Mux next value
		int temp = 0;
		mux[temp]++;
		while (mux[temp] > 1) {
			mux[temp] = 0;
			mux[temp + 1]++;
			temp++;
		}
		muxAddress++;
		if (muxAddress > 15) muxAddress = 0;
		// Update mux
		for (int i = 0; i < 4; i++) digitalWrite(22 + i, (mux[i] == 1) ? HIGH : LOW);
	}
	resetMux();

	// Midi output
	for (util::MidiModule* &module : util::MidiModule::getModules()) {
		int address = module->getMidiAddress();
		if (address >= 0) {
			int value = module->getMidiValue();
			if (value >= 0) {
				Serial.print(address);
				Serial.print(";");
				Serial.println(value);
			}
		}
	}
}

int AddressableModule::readAnalogValue() {
	if (muxAddress == this->getAddress().getAddress()) this->value = analogRead(this->getAddress().getPort());
	return this->value;
}

int AddressableModule::readDigitalValue() {
	if (muxAddress == this->getAddress().getAddress()) this->value = digitalRead(this->getAddress().getPort());
	return this->value;
}

void Led::work() {
	double br = this->getBrightness() * globalBrightness;
	Color color = Color(
			(int)((double) this->getColor().getRed() * br),
			(int)((double) this->getColor().getGreen() * br),
			(int)((double) this->getColor().getBlue() * br), this->getColor().getStrobe());
	bool asStrobe = color.getStrobe() > 0,
			toogle = (asStrobe && (((int) (millis() - this->time)) > color.getStrobe()));
	if ((!(this->last == color)) || toogle) {
		this->last = color;
		if (toogle || (!asStrobe && !this->strobe)) {
			this->time = millis();
			this->strobe = !this->strobe;
		}
		if (this->strobe) {
			leds.setPixelColor(this->getAddress(), color.getRed(), color.getGreen(), color.getBlue());
		} else {
			leds.setPixelColor(this->getAddress(), 0, 0, 0);
		}
		leds.show();
	}
}

void NumericEncoder::work() {
	if (muxAddress == this->a1.getAddress()) this->v1 = digitalRead(this->a1.getPort());
	if (muxAddress == this->a2.getAddress()) this->v2 = digitalRead(this->a2.getPort());
	if (this->v1 != -1 && this->v2 != -1) {
		if (this->v1 != this->last && this->v1 == 0) {
		    if (this->v2 == 1) {
		    	this->event(this, -1);
		    } else {
		    	this->event(this, 1);
		    }
		}
		this->last = this->v1;
		this->v1 = -1;
		this->v2 = -1;
	}
}


int asset::memory::getMemory(Key* key) {
	return -1;
}

void asset::memory::setupMemory(int memory) {
	if (memory > 0 && asset::memory::memory != memory) {
		for (module::MemoryModule* &a : asset::memory::getMemoryModules()) a->saveMemory();
		int location = (memory - 1) * asset::memory::MEMORY_SIZE;
		for (int k = 0; k < asset::memory::MEMORY_SIZE; k++) {
			EEPROM.update(location + k, readMemory(k * 8, (k + 1) * 8));
		}
		changeMemory(memory);
	}
}

void asset::memory::changeMemory(int memory) {
	asset::memory::memory = memory;
	int location = (memory - 1) * asset::memory::MEMORY_SIZE;
	for (int k = 0; k < asset::memory::MEMORY_SIZE; k++) {
		int b = EEPROM.read(k + location);
		updateMemory(k * 8, (k + 1) * 8 - 1, b);
	}
	for (module::MemoryModule* &a : asset::memory::getMemoryModules()) a->updateMemory();
}

int asset::memory::readMemory(int from, int to) { // 0 >= FROM > 512 ; 0 >= TO > FROM > 512
	if (from >= 0 && from < 512 && to >= from && to < 512) {
		int val = 0;
		for (int i = 0; i <= (to - from); i++) {
			val += ((int) pow(2, i)) * asset::memory::values[i + from];
		}
		return val;
	}
	return -1;
}

void asset::memory::updateMemory(int from, int to, int value) { // 0 >= FROM > 512 ; 0 >= TO > FROM > 512
	if (from >= 0 && from < 512 && to >= from && to < 512) {
		int val = int(value);
		for (int i = (to - from); i >= 0; i--) {
			int pw = pow(2, i);
			bool v = (val >= pw);
			asset::memory::values[i + from] = v;
			if (v) val -= pw;
		}
	}
}


/*void MidiNumericEncoder::work() {
	NumericEncoder::work();
}*/

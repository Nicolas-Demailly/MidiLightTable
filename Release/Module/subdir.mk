################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Module/Key.cpp \
../Module/Led.cpp \
../Module/Module.cpp \
../Module/NumericEncoder.cpp \
../Module/Potentiometer.cpp 

LINK_OBJ += \
./Module/Key.cpp.o \
./Module/Led.cpp.o \
./Module/Module.cpp.o \
./Module/NumericEncoder.cpp.o \
./Module/Potentiometer.cpp.o 

CPP_DEPS += \
./Module/Key.cpp.d \
./Module/Led.cpp.d \
./Module/Module.cpp.d \
./Module/NumericEncoder.cpp.d \
./Module/Potentiometer.cpp.d 


# Each subdirectory must supply rules for building sources it contributes
Module/Key.cpp.o: ../Module/Key.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/nicolas-demailly/eclipse/java-2020-12/eclipse//arduinoPlugin/packages/arduino/tools/avr-gcc/7.3.0-atmel3.6.1-arduino7/bin/avr-g++" -c -g -Os -Wall -Wextra -std=gnu++11 -fpermissive -fno-exceptions -ffunction-sections -fdata-sections -fno-threadsafe-statics -Wno-error=narrowing -MMD -flto -mmcu=atmega2560 -DF_CPU=16000000L -DARDUINO=10812 -DARDUINO_AVR_MEGA2560 -DARDUINO_ARCH_AVR     -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/cores/arduino" -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/variants/mega" -I"/home/nicolas-demailly/Arduino/libraries/ArduinoSTL-1.1.0/src" -I"/home/nicolas-demailly/Arduino/libraries/Adafruit_NeoPixel" -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/libraries/EEPROM/src" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 -x c++ "$<"   -o "$@"
	@echo 'Finished building: $<'
	@echo ' '

Module/Led.cpp.o: ../Module/Led.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/nicolas-demailly/eclipse/java-2020-12/eclipse//arduinoPlugin/packages/arduino/tools/avr-gcc/7.3.0-atmel3.6.1-arduino7/bin/avr-g++" -c -g -Os -Wall -Wextra -std=gnu++11 -fpermissive -fno-exceptions -ffunction-sections -fdata-sections -fno-threadsafe-statics -Wno-error=narrowing -MMD -flto -mmcu=atmega2560 -DF_CPU=16000000L -DARDUINO=10812 -DARDUINO_AVR_MEGA2560 -DARDUINO_ARCH_AVR     -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/cores/arduino" -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/variants/mega" -I"/home/nicolas-demailly/Arduino/libraries/ArduinoSTL-1.1.0/src" -I"/home/nicolas-demailly/Arduino/libraries/Adafruit_NeoPixel" -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/libraries/EEPROM/src" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 -x c++ "$<"   -o "$@"
	@echo 'Finished building: $<'
	@echo ' '

Module/Module.cpp.o: ../Module/Module.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/nicolas-demailly/eclipse/java-2020-12/eclipse//arduinoPlugin/packages/arduino/tools/avr-gcc/7.3.0-atmel3.6.1-arduino7/bin/avr-g++" -c -g -Os -Wall -Wextra -std=gnu++11 -fpermissive -fno-exceptions -ffunction-sections -fdata-sections -fno-threadsafe-statics -Wno-error=narrowing -MMD -flto -mmcu=atmega2560 -DF_CPU=16000000L -DARDUINO=10812 -DARDUINO_AVR_MEGA2560 -DARDUINO_ARCH_AVR     -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/cores/arduino" -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/variants/mega" -I"/home/nicolas-demailly/Arduino/libraries/ArduinoSTL-1.1.0/src" -I"/home/nicolas-demailly/Arduino/libraries/Adafruit_NeoPixel" -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/libraries/EEPROM/src" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 -x c++ "$<"   -o "$@"
	@echo 'Finished building: $<'
	@echo ' '

Module/NumericEncoder.cpp.o: ../Module/NumericEncoder.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/nicolas-demailly/eclipse/java-2020-12/eclipse//arduinoPlugin/packages/arduino/tools/avr-gcc/7.3.0-atmel3.6.1-arduino7/bin/avr-g++" -c -g -Os -Wall -Wextra -std=gnu++11 -fpermissive -fno-exceptions -ffunction-sections -fdata-sections -fno-threadsafe-statics -Wno-error=narrowing -MMD -flto -mmcu=atmega2560 -DF_CPU=16000000L -DARDUINO=10812 -DARDUINO_AVR_MEGA2560 -DARDUINO_ARCH_AVR     -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/cores/arduino" -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/variants/mega" -I"/home/nicolas-demailly/Arduino/libraries/ArduinoSTL-1.1.0/src" -I"/home/nicolas-demailly/Arduino/libraries/Adafruit_NeoPixel" -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/libraries/EEPROM/src" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 -x c++ "$<"   -o "$@"
	@echo 'Finished building: $<'
	@echo ' '

Module/Potentiometer.cpp.o: ../Module/Potentiometer.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/nicolas-demailly/eclipse/java-2020-12/eclipse//arduinoPlugin/packages/arduino/tools/avr-gcc/7.3.0-atmel3.6.1-arduino7/bin/avr-g++" -c -g -Os -Wall -Wextra -std=gnu++11 -fpermissive -fno-exceptions -ffunction-sections -fdata-sections -fno-threadsafe-statics -Wno-error=narrowing -MMD -flto -mmcu=atmega2560 -DF_CPU=16000000L -DARDUINO=10812 -DARDUINO_AVR_MEGA2560 -DARDUINO_ARCH_AVR     -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/cores/arduino" -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/variants/mega" -I"/home/nicolas-demailly/Arduino/libraries/ArduinoSTL-1.1.0/src" -I"/home/nicolas-demailly/Arduino/libraries/Adafruit_NeoPixel" -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/libraries/EEPROM/src" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 -x c++ "$<"   -o "$@"
	@echo 'Finished building: $<'
	@echo ' '



################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
/home/nicolas-demailly/Arduino/libraries/Adafruit_NeoPixel/Adafruit_NeoPixel.cpp 

C_SRCS += \
/home/nicolas-demailly/Arduino/libraries/Adafruit_NeoPixel/esp.c \
/home/nicolas-demailly/Arduino/libraries/Adafruit_NeoPixel/esp8266.c \
/home/nicolas-demailly/Arduino/libraries/Adafruit_NeoPixel/kendyte_k210.c \
/home/nicolas-demailly/Arduino/libraries/Adafruit_NeoPixel/rp2040.c 

C_DEPS += \
./libraries/Adafruit_NeoPixel/esp.c.d \
./libraries/Adafruit_NeoPixel/esp8266.c.d \
./libraries/Adafruit_NeoPixel/kendyte_k210.c.d \
./libraries/Adafruit_NeoPixel/rp2040.c.d 

LINK_OBJ += \
./libraries/Adafruit_NeoPixel/Adafruit_NeoPixel.cpp.o \
./libraries/Adafruit_NeoPixel/esp.c.o \
./libraries/Adafruit_NeoPixel/esp8266.c.o \
./libraries/Adafruit_NeoPixel/kendyte_k210.c.o \
./libraries/Adafruit_NeoPixel/rp2040.c.o 

CPP_DEPS += \
./libraries/Adafruit_NeoPixel/Adafruit_NeoPixel.cpp.d 


# Each subdirectory must supply rules for building sources it contributes
libraries/Adafruit_NeoPixel/Adafruit_NeoPixel.cpp.o: /home/nicolas-demailly/Arduino/libraries/Adafruit_NeoPixel/Adafruit_NeoPixel.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"/home/nicolas-demailly/eclipse/java-2020-12/eclipse//arduinoPlugin/packages/arduino/tools/avr-gcc/7.3.0-atmel3.6.1-arduino7/bin/avr-g++" -c -g -Os -Wall -Wextra -std=gnu++11 -fpermissive -fno-exceptions -ffunction-sections -fdata-sections -fno-threadsafe-statics -Wno-error=narrowing -MMD -flto -mmcu=atmega2560 -DF_CPU=16000000L -DARDUINO=10812 -DARDUINO_AVR_MEGA2560 -DARDUINO_ARCH_AVR     -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/cores/arduino" -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/variants/mega" -I"/home/nicolas-demailly/Arduino/libraries/ArduinoSTL-1.1.0/src" -I"/home/nicolas-demailly/Arduino/libraries/Adafruit_NeoPixel" -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/libraries/EEPROM/src" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 -x c++ "$<"   -o "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Adafruit_NeoPixel/esp.c.o: /home/nicolas-demailly/Arduino/libraries/Adafruit_NeoPixel/esp.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/nicolas-demailly/eclipse/java-2020-12/eclipse//arduinoPlugin/packages/arduino/tools/avr-gcc/7.3.0-atmel3.6.1-arduino7/bin/avr-gcc" -c -g -Os -Wall -Wextra -std=gnu11 -ffunction-sections -fdata-sections -MMD -flto -fno-fat-lto-objects -mmcu=atmega2560 -DF_CPU=16000000L -DARDUINO=10812 -DARDUINO_AVR_MEGA2560 -DARDUINO_ARCH_AVR     -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/cores/arduino" -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/variants/mega" -I"/home/nicolas-demailly/Arduino/libraries/ArduinoSTL-1.1.0/src" -I"/home/nicolas-demailly/Arduino/libraries/Adafruit_NeoPixel" -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/libraries/EEPROM/src" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"   -o "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Adafruit_NeoPixel/esp8266.c.o: /home/nicolas-demailly/Arduino/libraries/Adafruit_NeoPixel/esp8266.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/nicolas-demailly/eclipse/java-2020-12/eclipse//arduinoPlugin/packages/arduino/tools/avr-gcc/7.3.0-atmel3.6.1-arduino7/bin/avr-gcc" -c -g -Os -Wall -Wextra -std=gnu11 -ffunction-sections -fdata-sections -MMD -flto -fno-fat-lto-objects -mmcu=atmega2560 -DF_CPU=16000000L -DARDUINO=10812 -DARDUINO_AVR_MEGA2560 -DARDUINO_ARCH_AVR     -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/cores/arduino" -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/variants/mega" -I"/home/nicolas-demailly/Arduino/libraries/ArduinoSTL-1.1.0/src" -I"/home/nicolas-demailly/Arduino/libraries/Adafruit_NeoPixel" -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/libraries/EEPROM/src" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"   -o "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Adafruit_NeoPixel/kendyte_k210.c.o: /home/nicolas-demailly/Arduino/libraries/Adafruit_NeoPixel/kendyte_k210.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/nicolas-demailly/eclipse/java-2020-12/eclipse//arduinoPlugin/packages/arduino/tools/avr-gcc/7.3.0-atmel3.6.1-arduino7/bin/avr-gcc" -c -g -Os -Wall -Wextra -std=gnu11 -ffunction-sections -fdata-sections -MMD -flto -fno-fat-lto-objects -mmcu=atmega2560 -DF_CPU=16000000L -DARDUINO=10812 -DARDUINO_AVR_MEGA2560 -DARDUINO_ARCH_AVR     -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/cores/arduino" -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/variants/mega" -I"/home/nicolas-demailly/Arduino/libraries/ArduinoSTL-1.1.0/src" -I"/home/nicolas-demailly/Arduino/libraries/Adafruit_NeoPixel" -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/libraries/EEPROM/src" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"   -o "$@"
	@echo 'Finished building: $<'
	@echo ' '

libraries/Adafruit_NeoPixel/rp2040.c.o: /home/nicolas-demailly/Arduino/libraries/Adafruit_NeoPixel/rp2040.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"/home/nicolas-demailly/eclipse/java-2020-12/eclipse//arduinoPlugin/packages/arduino/tools/avr-gcc/7.3.0-atmel3.6.1-arduino7/bin/avr-gcc" -c -g -Os -Wall -Wextra -std=gnu11 -ffunction-sections -fdata-sections -MMD -flto -fno-fat-lto-objects -mmcu=atmega2560 -DF_CPU=16000000L -DARDUINO=10812 -DARDUINO_AVR_MEGA2560 -DARDUINO_ARCH_AVR     -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/cores/arduino" -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/variants/mega" -I"/home/nicolas-demailly/Arduino/libraries/ArduinoSTL-1.1.0/src" -I"/home/nicolas-demailly/Arduino/libraries/Adafruit_NeoPixel" -I"/home/nicolas-demailly/eclipse/java-2020-12/eclipse/arduinoPlugin/packages/arduino/hardware/avr/1.8.3/libraries/EEPROM/src" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -D__IN_ECLIPSE__=1 "$<"   -o "$@"
	@echo 'Finished building: $<'
	@echo ' '


